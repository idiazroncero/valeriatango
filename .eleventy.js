const { DateTime } = require("luxon");
const CleanCSS = require("clean-css");
const UglifyJS = require("uglify-js");
const htmlmin = require("html-minifier");
const showdown = require("showdown");
const converter = new showdown.Converter();

module.exports = function(eleventyConfig) {

  eleventyConfig.addLayoutAlias("post", "layouts/post.njk");

  // Date formatting (human readable)
  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("dd LLL yyyy");
  });

  eleventyConfig.addFilter("markdownify", string => {
    var html = converter.makeHtml(string);
    return html;
  });

  eleventyConfig.addFilter("respImg", sourceFile => {
    var sourcePointSplit = sourceFile.split('.')
    var sourceBarSplit = sourceFile.split('/')
    var sourceExt = sourcePointSplit[1];
    var sourceFullName = sourceBarSplit[sourceBarSplit.length - 1];
    var sourceName = sourceFullName.split('.')[0];
    var sourceDirArray = sourceBarSplit.filter(function(item, index){
      return index < (sourceBarSplit.length - 1)
    });
    var sourceDir = sourceDirArray.join('/');
    var sizes = {
      small: sourceDir + sourceName + '-small.' + sourceExt,
      smallWebp: sourceDir + sourceName + '-small.webp',
      medium: sourceDir  + sourceName + '-medium.' + sourceExt,
      mediumWebp: sourceDir  + sourceName + '-medium.webp',
      large: sourceDir + sourceName + '-large.' + sourceExt,
      largeWebp: sourceDir + sourceName + '-large.webp',
    }
    var html = `<picture>
        <source type="image/webp"
          srcset="${ sizes.smallWebp } 480w,
                  ${ sizes.mediumWebp } 800w,
                  ${ sizes.largeWebp } 1400w"
          sizes = "100W"
          />
        <source 
          srcset="${ sizes.small } 480w,
                  ${ sizes.medium } 800w,
                  ${ sizes.large } 1400w"
          sizes = "100W"
          />
        <img src="${ sizes.large }" />
      </picture>`
    return html;
  });

  // Date formatting (machine readable)
  eleventyConfig.addFilter("machineDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("yyyy-MM-dd");
  });

  // Minify CSS
  eleventyConfig.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  });

  // Minify JS
  eleventyConfig.addFilter("jsmin", function(code) {
    let minified = UglifyJS.minify(code);
    if( minified.error ) {
      console.log("UglifyJS error: ", minified.error);
      return code;
    }
    return minified.code;
  });
  // Minify HTML output
  eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
    if( outputPath.indexOf(".html") > -1 ) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: false,
        collapseBooleanAttributes: true,
        collapseInlineTagWhitespace: false,
        sortAttributes: true,
        sortClassName: true
      });
      return minified;
    }
    return content;
  });

  // only content in the `posts/` directory
  eleventyConfig.addCollection("posts", function(collection) {
    return collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/src\/posts\//) !== null;
    });
  });

  // only content in the `posts/` directory
  eleventyConfig.addCollection("cursos_grupales", function(collection) {
    var raw = collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/src\/clases\//) !== null;
    });
    var posts = raw.filter(function(item){
      return item.data.type == 'Grupales';
    })
    return posts;
  });

  eleventyConfig.addCollection("cursos_individuales", function(collection) {
    var raw = collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/src\/clases\//) !== null;
    });
    var posts = raw.filter(function(item){
      return item.data.type == 'Individuales';
    })
    return posts;
  });

  eleventyConfig.addCollection("cursos_intensivos", function(collection) {
    var raw = collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/src\/clases\//) !== null;
    });
    var posts = raw.filter(function(item){
      return item.data.type == 'Intensivos';
    })
    return posts;
  });

  eleventyConfig.addCollection("menu", function(collection) {
    var sortedMenu = collection.getFilteredByTag('nav').sort(function(a, b){
      return a.data.weight > b.data.weight;
    });
    return sortedMenu;
  });

  eleventyConfig.addCollection("destacadas", function(collection) {
    var destacadas = collection.getAll().filter(function(item){
      return item.data.destacar_portada == true;
    });
    if(destacadas[0]) {
      return [ destacadas[0] ];
    } else {
      return [];
    }
  });

  eleventyConfig.addShortcode("image", function(source, size, alt) {
    var sanitizedAlt = alt.replace('"', '\'');
    return `<img src="${source}" alt="${sanitizedAlt}"/>`;
  });

  // Don't process folders with static assets e.g. images
  eleventyConfig.addPassthroughCopy("src/assets");
  eleventyConfig.addPassthroughCopy("src/admin");
  eleventyConfig.addPassthroughCopy("src/public");
  eleventyConfig.addPassthroughCopy("src/favicon.ico");
  eleventyConfig.addPassthroughCopy("src/favicon.png");

  /* Markdown Plugins */
  let markdownIt = require("markdown-it");
  let options = {
    html: true,
    breaks: true,
    linkify: true
  };
  let opts = {
    permalink: true,
    permalinkClass: "direct-link",
    permalinkSymbol: "#"
  };

  return {
    templateFormats: [
      "md",
      "njk",
      "html"
    ],

    // If your site lives in a different subdirectory, change this.
    // Leading or trailing slashes are all normalized away, so don’t worry about it.
    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for URLs (it does not affect your file structure)
    pathPrefix: "/",

    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      input: "src",
      includes: "_includes",
      data: "_data",
      output: "dist"
    }
  };
};
