'use strict';

// Import and setup
const {dest, src, series, parallel, watch} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const shell = require('child_process').exec;
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const webp = require('gulp-webp');
const responsive = require('gulp-responsive');
const del = require('del');
sass.compiler = require('node-sass');

// CSS/SASS Compilation
function compileSass() {
  return src('./src/assets/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(dest('./src/assets/css'));
};

function watchSass() {
  return watch('./src/assets/scss/**/*.scss', compileSass );
};


// KSS Styleguide
function compileStyleguideSass() {
    return src('./src/assets/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceComments: true,
            outputStype: 'expanded'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(dest('./src/assets/css/styleguide/')); 
}

function compileStyleguide(cb) {
    shell('yarn kss --source src/assets/css/styleguide --destination docs --css ../src/assets/css/styleguide/styleguide.css --builder node_modules/huesos/src/kss-src', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    })
}


// Image optimization, resizing, etc

function cleanImages() {
    return del([
        'src/public/images/**/*.webp',
        'src/public/images/**/*-thumb.{png,jpg,jpeg,webp}',
        'src/public/images/**/*-small.{png,jpg,jpeg,webp}',
        'src/public/images/**/*-medium.{png,jpg,jpeg,webp}',
        'src/public/images/**/*-large.{png,jpg,jpeg,webp}',
    ])
};



function minifyImages() {
    return src('src/public/images/**/*')
        .pipe(imagemin())
		.pipe(dest('src/public/images'))
};

function resizeImages() {
    return src([
            'src/public/images/*.{png,jpg,jpeg,webp}', 
            '!src/public/images/*-small.{png,jpg,jpeg,webp}',
            '!src/public/images/*-large.{png,jpg,jpeg,webp}',
        ])
        .pipe(responsive({
            '**/*.{png,jpg,jpeg,webp}': 
                [
                    {
                        width: 480,
                        rename: function(path) {
                            path.basename += '-small';
                            return path;
                        }
                    },
                    {
                        width: 1400,
                        rename: function(path) {
                            path.basename += '-large';
                            return path;
                        }
                    },
                ] // End **/*.{png,jpg,webp}
            },
            // Globals
            {  
                withoutEnlargement: true,
                skipOnEnlargement: true,
                errorOnEnlargement: false,
                quality: 85,
                progressive: true,
                withMetadata: false,
            }
        ))
        .pipe(dest('src/public/images/'));
}

// Validators


// Build Helpers
function runEleventy(cb) {
    shell('yarn eleventy', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    })
}



// Define publicly available tasks
exports.sass = compileSass;
exports.sassWatch = watchSass;
exports.minifyImages = minifyImages;
exports.resizeImages = resizeImages;
exports.cleanImages = cleanImages;

// Grouped tasks
exports.styleguide = compileStyleguide;
exports.fullStyleguide = series(compileStyleguideSass, compileStyleguide);
exports.processImages = series(cleanImages, resizeImages, minifyImages);

// Main build process. Respect the order!
exports.build = series(
    series(resizeImages, minifyImages),
    runEleventy
)