
 function scrollTo(element, to, duration) {
     var start = element.scrollTop,
         change = to - start,
         currentTime = 0,
         increment = 20;
         
     var animateScroll = function(){        
         currentTime += increment;
         var val = Math.easeInOutQuad(currentTime, start, change, duration);
         element.scrollTop = val;
         if(currentTime < duration) {
             setTimeout(animateScroll, increment);
         }
     };
     animateScroll();

    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);

 }
 
 //t = current time
 //b = start value
 //c = change in value
 //d = duration
 Math.easeInOutQuad = function (t, b, c, d) {
   t /= d/2;
     if (t < 1) return c/2*t*t + b;
     t--;
     return -c/2 * (t*(t-2) - 1) + b;
 };



$(function () {

    $('.volver').on('click', function(e){
        e.preventDefault();
        scrollTo(document.documentElement, 0, 400);
    });

    $('.main-menu a').on('click', function(e){
        if(!$(this).attr('data-anchor')) { return }
        var position = $( '#' + $(this).attr('data-anchor') ).offset().top;
        e.preventDefault();
        scrollTo(document.documentElement, position, 400);
    });
});
  
  