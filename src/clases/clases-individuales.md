---
title: CLASES PRIVADAS
type: Individuales
main_image: /public/images/banner-clases-privadas-web-vale.jpg
summary: A tu medida! Puedes venir sólo, en pareja o en un pequeño grupo. +Info...
body2: L﻿as Clases Individuales nos brindan la posibilidad de trabajar en
  conjunto para diseñar un programa personalizado, que abarque desde tus deseos
  y necesidades, hasta las propuestas que yo, desde mi experiencia, pueda
  sugerirte.
body3: Al finalizar la clase reproduzco los ejercicios realizados en clase (y
  habitualmente añado algunos nuevos) para que puedas grabarlos y practicar en
  casa, ya que, en tango, como en cualquier otra disciplina, la repetición
  consciente de un movimiento va a permitirnos incorporar un nuevo gesto a
  nuestra memoria corporal.
horarios: |-
  

  * Horarios flexibles.
  * E﻿l Portón: c/José Calvo, 22.
---


- - -

****
