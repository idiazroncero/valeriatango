---
title: INICIACIÓN AL TANGO
type: Grupales
main_image: /public/images/banner-clases-matinales-web-vale-large.jpeg
summary: Te ayudamos a dar tus primeros pasitos en el 2x4 + Info...
body2: >-
  **A﻿ QUIÉNES VAN DIRIGIDAS ESTAS CLASES?**


  A﻿ todos los que quieran empezar a bailar tango argentino desde cero. No necesitas experiencia previa ni es necesario apuntarse en parejas.


  **Q﻿UÉ VAMOS A APRENDER?**


  E﻿n este primer curso vamos a trabajar las herramientas básicas que te permitan bailar de forma improvisada, en una pista de baile y rodeada/o de muchas otras parejas. LA idea es generar un lenguaje corporal que te permita conectar y bailar con otra persona, aunque no la conozcas, incluso, aunque no hablen si quiera el mismo idioma!
body3: >-
  Las clases son de **1 hora y media** de duración, de las cuales 1 hora la
  dedicamos a trabajar material nuevo, y media hora la utilizamos para hacer una
  **Práctica Guiada conjunta**.


  En la **Práctica** no se trabaja material nuevo, sino que todos los alumnos (de los distintos niveles), se reúnen a bailar, poner en práctica lo aprendido, y confraternizar en un ambiente cálido y familiar. Y es **Guiada**, porque tanto Caro como yo estamos pendientes y disponibles para responder a las dudas que surjan en el baile. a 


  D﻿esde nuestro punto de vista, el espacio de práctica es fundamental para afianzar los conocimientos adquiridos!
horarios: |-
  * Miércoles 19:30 a 21hs
  * E﻿l Portón: c/José Calvo, 22
---
- - -

- - -