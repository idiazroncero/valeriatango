---
title: INTERMEDIOS I
type: Grupales
main_image: /public/images/banner-clases-matinales-web-vale-large.jpeg
summary: Para afianzar lo aprendido. +Info...
body2: >-
  **A QUIÉN VAN DIRIGIDAS ESTAS CLASES?** 


  A﻿ todos aquellos que ya tienen nociones básicas de tango (que lleven por lo menos un año de clases), y que, si bien conocen ya una buena cantidad de elementos, les falta integrarlos en el baile. También a quienes han bailado hace tiempo, lo han dejado y quieren retomarlo. No es necesario apuntarse en parejas, pero sí que nos avises antes de venir a tu primera clase.


  **QUÉ VAMOS A APRENDER?** 


  En estas clases trabajamos sobre la **conexión** con nosotros mismos y con el compañero, que amplía nuestra sensibilidad y la percepción del otro (a través de ejercicios individuales, y en pareja), la **repetición** de estructuras, que nos permite asimilar un nuevo lenguaje corporal, y la **combinación de elementos**, para ganar fluidez.


  Siempre teniendo en cuenta el espacio en el que nos movemos (en la pista de baile estamos rodeados de otros bailarines) y las características particulares de la orquesta que estemos bailando.
body3: >-
  Las clases son de **1 hora y media** de duración, de las cuales 1 hora la
  dedicamos a trabajar material nuevo, y media hora la utilizamos para hacer una
  **Práctica Guiada conjunta**.


  E﻿n el momento de Práctica no se trabaja material nuevo, sino que todos los alumnos (de los distintos niveles), se reúnen a bailar, poner en práctica lo aprendido, y confraternizar en un ambiente cálido y familiar. Y es Guiada, porque tanto Caro como yo estamos pendientes y disponibles para responder a las dudas que surjan en el baile.


  D﻿esde nuestro punto de vista, el espacio de práctica es fundamental para afianzar los conocimientos adquiridos!
horarios: |-
  * L﻿unes de 19:30 a 21hs
  * Jueves de 11 a 12:30hs
  * E﻿l Portón:c/José Calvo, 22
---


- - -

****
