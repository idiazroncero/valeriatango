---
title: ESPECIALES TEMÁTICAS
type: Intensivos
main_image: /public/images/banner-clases-matinales-web-vale.jpg
summary: Impartidos por Valeria Cuenca y Carolina Gonzalez +Info...
body2: >-
  Son **clases especiales** de 2 horas de duración que daremos **Caro Gonzales y
  Valeria Cuenca** un sábado al mes. En cada clase desarrollamos una temática
  específica. Son clases pensadas para ambos roles de la pareja, aunque no es
  necesario apuntarse de a dos. Estas clases coincidirán con el mismo Sábado, y
  en las dos horas previas a la Práctica **EL YEITE**. La primera del curso
  24/25...el sábado **26 de Octubre**!!! Agendad la fecha!!! 


  Para más info, y para apuntarse, contactarnos por WhatsApp a los tel. **608.239.906** (Caro) o **655.225.930** (Vale)
body3: ""
horarios: |-
  * ¡PRÓXIMA FECHA: 26 DE OCTUBRE!
  * El Portón: calle José Calvo, 22
---


- - -

****
