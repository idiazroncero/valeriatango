---
title: TANGO DE BODAS
type: Individuales
main_image: /public/images/banner-clases-privadas-web-vale.jpg
summary: Les ayudo a bailar "el tango de sus vidas". +Info
body2: S﻿e trata de un pack de 8 clases de 1 hora, durante las cuales preparo
  una coreografía a medida de los novios, basándome en el tango que hayan
  elegido para bailar en su boda (y si no lo tienen elegido, les ayudo también
  en esa búsqueda!). Durante los primeros 4 o 5 encuentros, les voy
  transmitiendo las secuencias paso a paso. Y los últimos los dedicamos para
  limpiar, pulir y terminar de memorizar la coreografía. Para que en el momento
  de bailar, disfruten al máximo de ese momento mágico.
body3: >-
  A﻿l finalizar cada clase, hacemos un video del material trabajado ese día,
  para que puedan practicar hasta el siguiente encuentro. 


  S﻿i tienes cualquier duda o quieres informarte mejor, puedes llamarme al **+34 655.225.930** (también WhatsApp)
horarios: |-
  * H﻿orarios flexibles
  * E﻿l Portón: c/José Calvo, 22
---
- - -

- - -