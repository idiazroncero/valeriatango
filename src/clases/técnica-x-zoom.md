---
title: INTERMEDIOS II
type: Grupales
main_image: /public/images/banner-clases-matinales-web-vale-large.jpeg
summary: Para que encuentres tu propio tango. +Info...
body2: >-
  **A QUIÉN VAN DIRIGIDAS ESTAS CLASES?** 


  A﻿ aquellos que ya llevan tiempo bailando, y quieren ampliar su repertorio de recursos a la hora de bailar de forma improvisada en la milonga. No es necesario apuntarse en parejas, pero sí que nos avises antes de venir a tu primera clase.


  **QUÉ VAMOS A APRENDER?** 


  En estas clases trabajamos sobre tres principios básicos y complementarios: la **conexión** con nosotros mismos y con el compañero, que amplía nuestra sensibilidad y la percepción del otro, la **repetición** de estructuras, que nos permite asimilar un nuevo lenguaje corporal, y el **análisis de posibilidades** que nos permite trascender las estructuras aprendidas y generar nuestro propio baile.
body3: >-
  Las clases son de **1 hora y media** de duración, de las cuales 1 hora la
  dedicamos a trabajar material nuevo, y media hora la utilizamos para hacer una
  **Práctica Guiada conjunta**.


  E﻿n el momento de Práctica no se trabaja material nuevo, sino que todos los alumnos (de los distintos niveles), se reúnen a bailar, poner en práctica lo aprendido, y confraternizar en un ambiente cálido y familiar. Y es Guiada, porque tanto Caro como yo estamos pendientes y disponibles para responder a las dudas que surjan en el baile.


  D﻿esde nuestro punto de vista, el espacio de práctica es fundamental para afianzar los conocimientos adquiridos!
horarios: |-
  * L﻿unes de 20:30 a 22hs
  * E﻿l Portón: c/José Calvo, 22
---


- - -

****
