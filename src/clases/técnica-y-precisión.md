---
title: TÉCNICA FEMENINA
type: Intensivos
main_image: /public/images/banner-clases-web-vale-large.jpeg
summary: Talleres mensuales destinados a perfeccionar el rol seguidor. +Info...
body2: >-
  **EN QUÉ CONSISTE?**


  Cada taller dura 3 horas y media. Durante las tres primeras, desarrollamos la temática correspondiente al taller, a través de ejercicios individuales y grupales. Y la última media hora, la utilizamos para relajarnos después del trabajo físico, conversar, e intercambiar ideas con los compis, acompañados de un merecido "aperitivo".


  Las temáticas son evolutivas pero independientes: cada taller comienza, desarrolla y finaliza un tema determinado. Por ello, se puede tomar un taller aislado, pero si se toman correlativos, los primeros funciona como buena base para los que siguen. 


  Durante los talleres, ejemplifico los ejercicios que estamos trabajando proyectando videos de distintos bailarines "en acción", y al final de cada taller hago un pequeño resumen del material trabajado, para que puedan grabarlo y llevárselo a casa.
body3: ""
horarios: |-
  * ¡﻿¡¡RETOMAREMOS EN BREVE!!!
  * El Portón: C/ José Calvo, 22
---
## "Tango Aperitivo"

Es un Ciclo de Talleres Mensuales. En cada taller trabajamos una temática específica de tango desde la perspectiva del rol seguidor.

- - -

****
