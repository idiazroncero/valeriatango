---
title: Tango a tu medida
video_link: 'https://youtu.be/VYzqIfpG-7E'
facebook: 'https://m.facebook.com/valeria.l.cuenca'
youtube: 'https://youtu.be/x6d1dEkz8oI'
instagram: ''
front_image_qsoy: /public/images/santidonaire_valeria_web_-21-large.jpeg
front_image_1: /public/images/santidonaire_valeria_web_-5-large.jpeg
front_image_2: /public/images/santidonaire_valeria_web_-49-large.jpeg
front_image_3: /public/images/santidonaire_valeria_web_-13-cortada.jpg
front_image_4: /public/images/santidonaire_valeria_web_-20-large.jpeg
front_image_5: /public/images/santidonaire_valeria_web_-15-large.jpeg
---
Hola! Me llamo Valeria Cuenca, soy profesora y bailarina de tango argentino desde hace más de 20 años. 

Empecé a bailar tango a los 16 años en Buenos Aires, mi cuidad natal. En aquel momento no era una danza tan popular como ahora, pero ya se estaba gestando un gran cambio en la forma de entender el baile, gracias a maestros como Gustavo Naveira, Fabián Salas y Mariano Chicho Frúmboli, con quienes tuve el placer de aprender. El tango me atrapó enseguida, tanto por el baile en sí como por la gente que me llevó a conocer. A partir de entonces se transformó en mi compañero de viaje!

Aprendí muchas cosas a través del tango, pero sobre todo, descubrí que me encanta enseñar! Doy clases desde 1996. Los primeros años, en Argentina, de la mano de mi maestra y amiga, Dina Martínez, y luego, aquí en España, donde vivo desde 2004. He viajado por Europa como bailarina y profesora de tango, fundamentalmente junto a Fernando Nahmijas, bailarín con quién he compartido 14 años de trayectoria. Paralelamente me he formado en diversas disciplinas (yoga, contact improvisation, danza contemporánea, trabajo corporal), para ampliar mis conocimientos sobre el movimiento, y de esa forma enriquecer mis clases. En la actualidad, me dedico especialmente a aquello que más me gusta: enseñar, trasmitir los engranajes de esta danza pícara, valerosa, y un poco irreverente.
