---
title: '1/02 - INTENSIVO PARA PAREJAS: "COLGADAS Y VOLCADAS"'
destacar: true
destacar_portada: false
featured_image: /public/images/taller-vale-mariana_fuera-de-eje_completo.jpg
date: 2020-01-24T13:05:57.159Z
summary: '- 2º TALLER DE TANGO PARA PAREJAS DEL CICLO 2019/20 -'
tags:
  - post
---
El próximo Sábado **1 de Febrero** continuamos el **Ciclo de INTENSIVOS DE TANGO PARA PAREJAS**, junto a **Mariana Ancarola**. En cada fecha trabajaremos una temática específica de tango visto desde los dos roles, conductor y seguidor.

La temática de este taller es **"FUERAS DE EJE: COLGADAS Y VOLCADAS"**. En él analizaremos qué es un "fuera de eje", cómo proponerlo y compensarlo, las respuestas posibles de la pierna libre, y su aplicación en el baile de pista.

Serán 3 horas de trabajo técnico y luego nos tomaremos media hora más para practicar lo trabajado, y para compartir un merecido aperitivo!

No es necesario apuntarse en pareja, sin embargo si se apuntan chicas o chicos solos, quedarán en lista de espera hasta que se apunte alguien del otro rol con quien puedan hacer el taller.

Por cualquier duda o para reservar plaza, pueden contactar a **info@valeriatangomadrid.com** o al **655.225.930** (también WhatsApp). 

\- Plazas limitadas -

- - -

**"FUERAS DE EJE: COLGADAS Y VOLCADAS"**

\-con Mariana Ancarola y Valeria Cuenca-

**SÁBADO 1 DE FEBRERO**

de 11 a 14hs - **Clase**

de 14 a 14:30hs - **Práctica Guiada + Aperitivo**

**Dónde?** en El Portón, calle José Calvo, 22 (metro Francos Rodriguez L7 o Estrecho L1)

**Precio:** €35 por persona o €60 la pareja, si se apuntan juntos.

- - -

**PRÓXIMOS INTENSIVOS PARA PAREJAS:**

28 de Marzo: "GANCHOS DE ELLA Y DE ÉL"

9 de Mayo: "BARRIDAS, LAS OLVIDADAS DEL BAILE"

13 de Junio: "BOLEOS, MARCA Y RECEPCIÓN"
