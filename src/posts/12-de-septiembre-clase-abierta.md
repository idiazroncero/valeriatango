---
title: LUNES 12/9, CLASE ABIERTA DE DE TANGO!!! ¡TE ESPERAMOS!
destacar: true
destacar_portada: false
featured_image: /public/images/20220906_105038_0000.png
date: 2022-09-10T07:32:09.979Z
summary: ¡Volvemos al ruedo!
tags:
  - post
---
Tengo la enorme alegría de contarles que, a partir de este mes de Septiembre, vuelvo a dar clases de tango en Madrid, y esta vez junto a mi querida amiga y gran maestra Caro González! El lunes que viene, **12 de Septiembre**, daremos una **CLASE ABIERTA** a la que están todos invitados! Y a partir de ahí comenzaremos las clases regulares. Esperamos verlos muy prontito!!! 



**CLASE ABIERTA: Lunes 12/9 de 19:30 a 21hs** Por favor, apúntate previamente para reservar tu lugar!

**\
CLASES REGULARES**

**A partir del 19 de Septiembre**\
19:30 a 20:30hs......**INICIACIÓN**\
20:30 a 21Ss..........**PRÁCTICA CONJUNTA**

21 a 22hs...............**INTERMEDIOS**

**A partir de Octubre también clases por las mañanas!**\
11 a 12hs.................**INICIACIÓ​N**\
12 a 12:30Ss...........**PRÁCTICA CONJUNTA**

12:30 a 13:30hs......**INTERMEDIOS**

**\
Dónde?** En El Portón, calle Jose Calvo 22, Madrid (metro Francos Rodriguez L7 o Estrecho L1, un poquito más lejos)

**Precio**: Clase abierta, gratuita

            Iniciación €45 la mensualidad por persona (clase + práctica conjunta)

            Intermedios €50 la mensualidad por persona (clase + práctica)

**Para más info** o apuntarte contacta nos: **+34 608.239.906** (Caro), +**34 655.225.930** (Vale)