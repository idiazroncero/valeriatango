---
title: 13/1 "INTENSIVO DE INICIACIÓN AL TANGO"
destacar: false
destacar_portada: false
featured_image: /public/images/enganchate24.jpg
date: 2024-01-03T07:20:04.271Z
summary: ¡¡¡(RE) ENGÁNCHATE AL TANGO!!!
tags:
  - post
---
Si uno de tus propósitos para el 2024 es aprender a bailar tango, o si estás tomando clases y quieres recordar las bases, este taller está hecho a tu medida! Serán tres horas de clase en las que trabajaremos los elementos básicos de conexión, abrazo, caminata y algo más...En este taller te daremos las herramientas elementales para bailar tango argentino. A partir de allí, vas a tener una base que te permitirá, si te ha gustado y quieres seguir, "engancharte" a clases grupales, aunque ya esté el curso empezado.   

**(RE) ENGÁNCHATE AL TANGO**

**Cuándo?** Sábado 13 de enero de 16 a 19hs.

**Donde?** En el Portón Rojo, calle José Calvo, 22-Madrid (metro Francos Rodríguez L7)

**Precio:** €35 (€60 si te apuntas en pareja)

Para reservar plazas o por cualquier otra info, puedes escribirme a **info@valeriatangomadrid.com** o por WhatsApp al **655.225.930**