---
title: '14/12: "EL BOLEO Y SUS VERICUETOS"'
destacar: true
destacar_portada: false
featured_image: /public/images/boleos-wa-14.12.19.jpeg
date: 2019-12-05T14:13:10.999Z
summary: 3º TALLER DE TÉCNICA FEMENINA - CURSO 2019/20 -
tags:
  - post
---
Sábado 14 de Diciembre, último **Taller de Técnica Femenina** del año!!! Para que el 2020 no te pille sin saber bolear!!!!

Trabajaremos el principio de contradirección, los distintos tipos de boleos: circulares atrás, adelante, lineales; y las diferentes calidades de boleos según el impulso y la rítmica propuesta.

Dedicamos tres horas para el **trabajo técnico**, y a continuación nos tomamos media hora de un merecido **Aperitivo** donde aprovechamos para conocernos un poco más, intercambiar impresiones, y grabar un resumen del material trabajado en clase.

Para **+ Info** o apuntarte puedes contactarme a **info@valeriatangomadrid.com**, o al **655.225.930** (también WhatsApp)

**\-Plazas limitadas-**

- - -

**Cuándo?** Sábado 14 de Diciembre de 11 a 14:30hs.

**Dónde?** En El Portón, calle José Calvo, 22 Madrid. Metro Francos Rodriguez L7 o Escrecho L1

**Precio?** €35 por persona.

- - -

**"TALLERES DE TÉCNICA FEMENINA DE TANGO"**

**CRONOGRAMA CURSO 2019/20**

19/10 **Los secretos del Pivot**

23/11 **El Giro: elemento milonguero por excelencia**

14/12 **El Boleo y Sus Vericuetos**

18/1 **Adornos Rítmicos**

22/2 **Adornos Melódicos**

21/3 **Los secretos del Pivot** (repetición del taller de Octubre)

25/4 **El Giro: elemento milonguero por excelencia**
 (repetición del taller de Noviembre)

23/5 **Adornos Rítmicos y Melódicos** (versión reducida de los talleres de enero y febrero)
