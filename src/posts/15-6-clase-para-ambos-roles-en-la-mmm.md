---
title: '15/6: CLASE PARA AMBOS ROLES EN LA "MMM"'
destacar: true
featured_image: /public/images/mmm-15.6.19.jpeg
date: 2019-06-13T10:05:59.572Z
summary: 'Eje y Circunferencia, quién rodea a quién?'
tags:
  - post
---
Trabajaremos el código de marcha, diferentes formas de rodear al compañero, maneras de cambiar el eje, y su aplicación en la pista



La clase será de 18 a 19hs. Luego...a bailar valses y milongas con Antonio Carcavilla!!!



**Cuándo?** Sábado 15 de Junio de 18 a 22:30hs

**Dónde?** En A Escena, calle Sambara 21 - Madrid - (metro El Carmen)

**Precio?** Clase + Milonga + Consumición: €12 por persona

Para + Info contacta a "Las tres A" **618.327.652 / 670.826.261**
