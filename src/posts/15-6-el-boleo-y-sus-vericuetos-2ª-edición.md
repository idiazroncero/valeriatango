---
title: '15/6: EL BOLEO Y SUS VERICUETOS - 2ª EDICIÓN - '
destacar: true
destacar_portada: false
featured_image: /public/images/boleos-15.6.19.jpeg
date: 2019-06-07T12:48:58.749Z
summary: Ultimo Taller de Técnica Femenina del curso 2018/19!!!
tags:
  - post
---
Sábado 15 de Junio, último Taller de Técnica Femenina del curso 2018/19. A partir de Octubre tendremos muchos más!!!

Trabajaremos el principio de contradirección, los distintos tipos de boleos: circulares atrás, adelante, lineales; y las diferentes calidades de boleos según el impulso y la rítmica propuesta.

Para + Info o apuntarte puedes contactarme a info@valeriatangomadrid.com, o al 655.225.930 (también WhatsApp)

\-Plazas limitadas-

**Cuándo?** Sábado 15 de Junio de 11 a 14:30hs.

**Dónde?** En El Portón Rojo, calle José Calvo, 22 Madrid. Metro Francos Rodriguez L7 o Escrecho L1

**Precio?** €30 por persona.
