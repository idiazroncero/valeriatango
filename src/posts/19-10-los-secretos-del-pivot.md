---
title: 'VUELVEN LOS TALLERES!!!! 19/10: "LOS SECRETOS DEL PIVOT"'
destacar: true
destacar_portada: false
featured_image: /public/images/taller-los-secretos-del-pivot_2.jpg
date: 2019-10-11T12:15:50.835Z
summary: 1º TALLER DE TÉCNICA FEMENINA DEL CURSO 19/20!!!
tags:
  - post
---
**¡¡¡SÁBADO 19/10, 1º TALLER DE TÉCNICA FEMENINA DEL CURSO 19/20!!!**

**LOS SECRETOS DEL PIVOT**

Este taller y el que le sigue son fundamentales para comprender los pilares sobre los que se construye toda la estructura del baile desde el rol seguidor. 

La primera parte del taller trabajamos con ejercicios de base de eje, equilibrio ,postura y traslado; y luego se profundiza en las particularidades del pivot adelante y atrás, el sobrepivot, diferentes impulsos y variaciones de la técnica base. 

Dedicamos tres horas para el trabajo técnico, y a continuación nos tomamos media hora de un merecido Aperitivo donde aprovechamos para conocernos un poco más, intercambiar impresiones, y grabar un resumen del material trabajado en clase.

**Cuándo?** Sábado 19 de Octubre de 11 a 14:30hs.

**Dónde?** En El Portón, calle José Calvo, 22 (metro Francos Rodriguez L7, o Estrecho L1)

**Precio?** €35 por persona



Para + Info o apuntarte puedes contactarme a **info@valeriatangomadrid.com**, o al **655.225.930** (también WhatsApp)

\-Plazas limitadas-



**CRONOGRAMA CURSO 2019/20**

19/10 Los secretos del Pivot

23/11 El Giro: elemento milonguero por excelencia

14/12 El Boleo y Sus Vericuetos

18/1 Adornos Rítmicos

22/2 Adornos Melódicos

21/3 Los secretos del Pivot (repetición del taller de Octubre)

25/4 El Giro: elemento milonguero por excelencia

23/5 El Boleo y sus vericuetos

13/6 Adornos Rítmicos y Melódicos (versión reducida de los talleres de enero y febrero)
