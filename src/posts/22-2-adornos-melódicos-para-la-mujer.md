---
title: '22.2 - "ADORNOS MELÓDICOS": TALLER COMPLETO!!!'
destacar: true
destacar_portada: false
featured_image: /public/images/adornos-r.-taller-completo.jpeg
date: 2020-02-14T10:25:36.422Z
summary: TALLER DE TÉCNICA FEMENINA DE TANGO con Valeria Cuenca
tags:
  - post
---
TALLER DE TÉCNICA FEMENINA DE TANGO con Valeria Cuenca

En este taller prepararemos el cuerpo para hacer adornos, trabajaremos la densidad del movimiento, el peso de la cadera y su relación con la pierna libre, y veremos en qué momentos pueden tener lugar estos adornos .

De 11 a 14hs, nos dedicaremos al trabajo técnico, y a continuación de 14 a 14:30hs compartiremos un merecido Aperitivo y grabaremos un resumen del material trabajado en clase.

Para + Info o apuntarte puedes contactarme a info@valeriatangomadrid.com, o al 655.225.930 (también WhatsApp)

\-Plazas limitadas-

- - -

Cuándo? Sábado 22 de Febrero de 11 a 14:30hs.

Dónde? En El Portón, calle José Calvo, 22 Madrid. Metro Francos Rodriguez L7 o Escrecho L1

Precio? €35 por persona.

- - -

"TALLERES DE TÉCNICA FEMENINA DE TANGO"

CRONOGRAMA 2020

22/2 Adornos Melódicos

21/3 Los secretos del Pivot (repetición del taller de Octubre)

25/4 El Giro: elemento milonguero por excelencia (repetición del taller de Noviembre)

23/5 Adornos Rítmicos y Melódicos (versión reducida de los talleres de enero y febrero)

Y retomamos después de verano!!!
