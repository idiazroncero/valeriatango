---
title: '23/11: "EL GIRO, ELEMENTO MILONGUERO POR EXCELENCIA"'
destacar: true
destacar_portada: false
featured_image: /public/images/giros-23.11.19.jpeg
date: 2019-11-14T11:07:05.162Z
summary: '2º  TALLER DE TÉCNICA FEMENINA - CURSO 2019/20 - '
tags:
  - post
---
**¡¡¡SÁBADO 23/11, 2º TALLER DE TÉCNICA FEMENINA - CURSO 19/20!!!**

**"EL GIRO: ELEMENTO MILONGUERO POR EXCELENCIA"**

Este taller junto con el anterior (Los secretos del pivot) son fundamentales para comprender los pilares sobre los que se construye toda la estructura del baile desde el rol seguidor.

 Durante el Taller trabajamos el código de marcha, posiciones y rítmica predeterminada, particularidades del giro según el tipo de abrazo, y variaciones de la técnica base. 

Dedicamos tres horas para el trabajo técnico, y a continuación nos tomamos media hora de un merecido Aperitivo donde aprovechamos para conocernos un poco más, intercambiar impresiones, y grabar un resumen del material trabajado en clase.

**Cuándo?** Sábado 23 de Noviembre de 11 a 14:30hs.

**Dónde?** En El Portón, calle José Calvo, 22 (metro Francos Rodriguez L7, o Estrecho L1).

**Precio?** €35 por persona.

Para **+ Info** o apuntarte puedes contactarme a **info@valeriatangomadrid.com**, o al **655.225.930** (también WhatsApp)

\-Plazas limitadas-



**"TALLERES DE TÉCNICA FEMENINA DE TANGO"**

**CRONOGRAMA CURSO 2019/20**

19/10 Los secretos del Pivot

23/11 El Giro: elemento milonguero por excelencia

14/12 El Boleo y Sus Vericuetos

18/1 Adornos Rítmicos

22/2 Adornos Melódicos

21/3 Los secretos del Pivot (repetición del taller de Octubre)

25/4 El Giro: elemento milonguero por excelencia

23/5 El Boleo y sus vericuetos

13/6 Adornos Rítmicos y Melódicos (versión reducida de los talleres de enero y febrero)
