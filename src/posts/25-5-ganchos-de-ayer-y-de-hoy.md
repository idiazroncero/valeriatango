---
title: '25/5: GANCHOS DE AYER Y DE HOY'
destacar: true
destacar_portada: false
featured_image: /public/images/ganchos-25.5.19.jpeg
date: 2019-05-17T21:35:09.942Z
summary: 8º TALLER DE TÉCNICA FEMENINA
tags:
  - post
---
**SÁBADO 25 DE MAYO**, 8º Taller de Técnica Femenina: **"GANCHOS DE AYER Y DE HOY"**

Trabajaremos ganchos en traslado, en pivot, sobrepivots, el rebote y la disponibilidad de la pierna libre. 

**Cuándo?** Sábado 25/5 de 11 a 14:30hs. 

**Dónde?** En El Portón Rojo, calle José Calvo, 22 (Francos Rodriguez L7, Estrecho L1) 

**Precio?** €30

Para + info e inscripción, pueden contactarme a info@valeriatangomadrid.com o al 655.225.930 (también WhatsApp) **\-Plazas Limitadas-**
