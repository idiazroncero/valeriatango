---
title: '27/4: TALLER DE TÉCNICA FEMENINA - BARRIDAS -'
destacar: true
destacar_portada: false
featured_image: /public/images/barridas-27.4.19.jpeg
date: 2019-04-23T10:53:41.634Z
tags:
  - post
---
Sábado 27 de Abril, Taller de Técnica Femenina: "BARRIDAS, LAS OLVIDADAS DE L BAILE"



Trabajaremos el contacto y resistencia para dejar la pierna disponible, barridas históricas, y nuevas posibilidades de barridas femeninas y masculinas.



Plazas limitadas!!!



Para + Info o apuntarte puedes contactarme a info@valeriatangomadrid.com, o al 655.225.930 (también WhatsApp)



Cuándo? Sábado 27 de Abril de 11 a 14:30hs.



Dónde? En El Portón Rojo, calle José Calvo, 22 Madrid. Metro Francos Rodriguez L7 o Escrecho L1
