---
title: '30/11 - INTENSIVO PARA PAREJAS: "SACADAS DE ÉL Y DE ELLA"'
destacar: true
destacar_portada: false
featured_image: /public/images/sacadas-30.11.19.jpeg
date: 2019-11-24T23:33:45.668Z
summary: 1º TALLER PARA PAREJAS DEL CICLO 2019/20!!!
tags:
  - post
---
Este **Sábado 30 de Noviembre** iniciamos, junto con **Mariana** **Ancarola**, un nuevo **Ciclo de INTENSIVOS DE** **TANGO PARA PAREJAS**. En cada fecha trabajaremos una temática específica de tango visto desde los dos roles, conductor y seguidor.

La temática de este primer taller es **"SACADAS, DAR Y RECIBIR"**. En él trabajaremos sacadas del hombre y de la mujer, la marca, la mecánica de movimiento, y analizaremos las posibilidades de sacadas que tenemos en el baile. 

Serán 3 horas de trabajo técnico y luego nos tomaremos media hora más para practicar lo trabajado, y para compartir un merecido aperitivo! 

No es necesario apuntarse en pareja, sin embargo si se apuntan chicas o chicos solos, quedarán en lista de espera hasta que se apunte alguien del otro rol con quien puedan hacer el taller. 

Por cualquier duda o para reservar plaza, pueden contactar a **info@valeriatangomadrid.com** o al **655.225.930** (también WhatsApp). **\- Plazas limitadas -** 

- - -

**INTENSIVOS DE TANGO PARA PAREJAS
 -con Mariana Ancarola y Valeria Cuenca-**

Sábado 30 de Noviembre: **"SACADAS, DAR Y RECIBIR"**

**Horario:** de 11 a 14hs clase - de 14 a 14:30hs Práctica + Aperitivo

**Dónde?** en El Portón, calle José Calvo, 22 (metro Francos Rodriguez L7 o Estrecho L1)

**Precio:** €35 por persona o €60 la pareja, si se apuntan juntos. 





**PRÓXIMOS INTENSIVOS PARA PAREJAS:**

1 de Febrero: **"FUERAS DE EJE"** (colgadas / volcadas)

28 de Marzo: "**GANCHOS DE ELLA Y DE ÉL"**

9 de Mayo: "**BARRIDAS, LAS OLVIDADAS DEL BAILE"**
