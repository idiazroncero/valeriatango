---
title: 31/1 - PRIMERA TEMPRANERA DEL AÑO...Y ÉSTA ES MUY CURIOSA!!!
destacar: true
destacar_portada: false
featured_image: /public/images/tempranera-31.1.20-web.jpeg
date: 2020-01-28T09:43:06.973Z
summary: '- PRÁCTICA GUIADA DE TANGO POR LAS MAÑANAS -'
tags:
  - post
---
Este viernes, 31 de Enero, tenemos la primera "TEMPRANERA" de este año!!! Y en esta oportunidad, nuestro invitado habitual, el querido RAFAEL FLORES nos hace una propuesta muy especial: ping pong de preguntas y respuestas!!! Una oportunidad única para sacarnos todas las dudas que tenemos sobre historia del tango, sus orquestas, sus cantantes, estilos...Así que, preparen sus preguntas, que una hora se nos va a quedar corta!!! La idea será bailar de 11 a 13hs, y de 13 a 14hs, hacer el Ping Pong!

- - -

LA TEMPRANERA es una PRÁCTICA GUIADA DE TANGO POR LA MAÑANA. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

La Tempranera está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una Práctica Guiada porque yo, Vale, estoy presente y disponible para responder a las dudas que surjan, y corregir individualmente si alguien lo necesita.

Cuándo? El último viernes de cada mes, de 11 a 14hs.

Dónde? En El Portón, Calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

Precio? "A la Gorra", un sombrero donde cada uno pone lo que puede, si puede!
