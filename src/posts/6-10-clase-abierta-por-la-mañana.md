---
title: '6 de Octubre: "CLASE ABIERTA POR LA MAÑANA"!!!'
destacar: true
destacar_portada: false
featured_image: /public/images/atrévete.jpeg
date: 2022-10-03T10:31:41.674Z
summary: Atrévete con el Tango Mañanero!!!
tags:
  - post
---
Este próximo Jueves, 6 de Octubre, **CLASE ABIERTA Y GRATUITA** con **Caro González** y **Vale Cuenca!!!** Y a partir ahí, clases semanales de nivel inicial e intermedio, por las mañanas!!! Aquí abajo va toda la info!! **Atrévete con el tango mañanero!!!**

**\
CLASE ABIERTA Y GRATUITA: Jueves 6/10 de 11 a 12:30hs.** Por favor apúntate previamente para reservar tu lugar (+34 655 225 930/+34 608 239 906)



**CLASES REGULARES POR LAS MAÑANAS** (desde el 13/10) 

* 11 a 12hs.................**INICIACIÓ​N**
* 12 a 12:30hs.........**PRÁCTICA CONJUNTA**
* 12:30 a 13:30hs......**INTERMEDIOS**

**\
Dónde?** En El Portón, calle Jose Calvo 22, Madrid (metro Francos Rodriguez L7 o Estrecho L1)

**Precio**: *Clase abierta:* gratuita

 *\    Iniciación:* €45 la mensualidad por persona (clase + práctica conjunta)

 *\    Intermedios:* €50 la mensualidad por persona (clase + práctica)

**Para más info** o apuntarte contáctanos a los tel. **+34 608.239.906** (Caro), +**34 655.225.930** (Vale)