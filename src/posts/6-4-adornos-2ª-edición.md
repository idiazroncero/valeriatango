---
title: '6/4: TALLER DE TÉCNICA FEMENINA: "ADORNOS" -AFORO COMPLETO-'
destacar: true
destacar_portada: false
featured_image: /public/images/adornos-2ª-ed.-06.04.2019.jpg
date: 2019-03-28T14:15:56.082Z
summary: Por si te quedaste con las ganas en Diciembre!
tags:
  - post
---
Sábado 6 de Abril, Taller de Técnica Femenina: **"ADORNANDO NUESTRO BAILE"** - Segunda Edición -

Trabajaremos adornos en traslado, pivot y giro, diferentes calidades de movimiento, y casos "particulares".

Plazas limitadas!!!

Para + Info o apuntarte puedes contactarme a info@valeriatangomadrid.com, o al 655.225.930 (también WhatsApp)

**Cuándo?** Sábado 6 de Abril de 11 a 14:30hs.

**Dónde?** En El Portón Rojo, calle José Calvo, 22 Madrid. Metro Francos Rodriguez L7 o Escrecho L1
