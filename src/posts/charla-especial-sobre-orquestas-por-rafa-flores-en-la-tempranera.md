---
title: '25/10: CHARLA ESPECIAL DE RAFA FLORES EN "LA TEMPRANERA"!!!'
destacar: true
destacar_portada: false
featured_image: /public/images/logo-la-tempranera.jpg
date: 2019-10-24T10:01:14.365Z
summary: 'VIERNES 25/10, DE 11 A 14HS, VUELVE "LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO"'
tags:
  - post
---
Este viernes, **25 de Octubre**, vuelve **"La Tempranera"** y con novedades!!! Extendemos el horario (ahora de 11 a 14hs), estrenamos suelo (de madera!!! para bailar más a gustito!) Y, la guinda del pastel, viene **Rafael Flores Montenegro** a regalarnos una vez más una charla sobre tango!!!! En esta oportunidad, sobre **las Orquestas y sus cantantes! ¡¡¡IMPERDIBLE!!!!** 

****

**LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

La Tempranera está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una Práctica Guiada porque yo, Vale, estoy presente y disponible para responder a las dudas que surjan, y corregir individualmente si alguien lo necesita.

**Cuándo?** El último viernes de cada mes, de 11 a 14hs. 

**Dónde?** En El Portón, Calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

**Precio?** "A la Gorra", un sombrero donde cada uno pone lo que puede, si puede!
