---
title: '22/5: "FUNDAMENTOS" CON DINA MARTINEZ -AFORO COMPLETO-'
destacar: true
destacar_portada: false
featured_image: /public/images/taller-dina.jpeg
date: 2019-05-09T11:27:04.199Z
summary: '22 DE MAYO, ÚNICA FECHA, DOS TALLERES IMPRESCINDIBLES!!!'
tags:
  - post
---
**\-MIÉRCOLES 22 DE MAYO DE 20 A 23HS-**

## FUNDAMENTOS DEL TANGO

La propuesta de estos talleres es sentar las bases técnicas para lograr un baile confortable, orgánico y cómplice con el otro. Entendiendo cómo funciona nuestro cuerpo desde los elementos más básicos, podemos construir estructuras mucho más complejas a continuación. 

**_Taller 1 - 20.21:30hs: "SE HACE CAMINATA AL ANDAR"_**

Trabajaremos el Eje, la Conexión, el Abrazo, los Apoyos, la Pierna libre y la de base.

**_Taller 2-21:30.23hs: "COCINANDO ESPIRALES"_**

Entre espirales y pivots...conoces la diferencia? Aquí la develaremos! Además veremos diferentes formas de hacer torsión, el Centro y la Circunferencia.

**Cuándo?** Miércoles 22 de Mayo de 20 a 23hs.

**Dónde?** en El Portón Rojo, calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

**Precio?** €20 un solo taller- €35 si tomas los dos.

Estos talleres están abiertos a todos los niveles, y no es necesario apuntarse en parejas - Plazas limitadas -

Para más info y apuntarse, pueden contactar con Vale ainfo@valeriatangomadrid.com o al 655.225.930 (también WhatsApp)

**DINA MARTINEZ** es profesora de tango con una experiencia docente de más de 30 años, dirige en Buenos Aires la Escuela y Práctica de tango "El Motivo Tango" desde 2004 junto a Luciana Valle y Valencia Batiuk, y coordina la Compañía de Tango Besos Tintos. [www.dinamartineztango.tk](http://dinamartineztango.tk/)
