---
title: "MARZO`23: ¡¡¡DINA MARTINEZ VUELVE A MADRID!!!"
destacar: false
destacar_portada: false
featured_image: /public/images/dina-madrid-23.jpeg
date: 2023-02-22T06:22:52.563Z
summary: "- GIRA EUROPEA MARZO DE 2023 -"
tags:
  - post
---
En esta oportunidad Dina nos ofrece 4 talleres de tango imperdibles, dos de ellos de técnica individual y los otros dos, para parejas. Aquí va la programación.

**MARTES 21 DE MARZO:**

**19.30 a 21hs. Fundamentos I : "CONEXIÓN Y MOTORES DE MOVIMIENTO".** Trabajaremos la Conexión con el propio cuerpo y con el otro, Postura, Ubicación del Eje, Motores de Movimiento, Pierna de Base/Pierna Libre y Pivots. Este taller es de técnica individual (para chicos y chicas, pero no es necesario apuntarse en parejas)

**21 a 22.30hs. "CAMBIOS DE DIRECCIÓN".** Trabajaremos las posibilidades de este elemento que, a la vez que sencillo, es profundamente dinámico!!

- - -

**MIERCOLES 22 DE MARZO:**

**19.30 a 21hs. Fundamentos II : "ESPIRALES".** En esta clase trabajaremos los espirales, la diferencia entre Moverse desde Abajo o Moverse desde Arriba, la relación Torso-Cadera-Pierna libre, Centro y Circunferencia, y el Comportamiento de cada rol. Este taller es de técnica individual (para chicos y chicas, pero no es necesario apuntarse en parejas)

**21 a 22.30hs. "GIROS DE EJE COMPARTIDO".** En esta clase trabajaremos sobre este elemento que se siente delicioso y es perfecto para pistas muy concurridas!!

- - -

**Dónde?** en El Portón Rojo, calle José Calvo 22, Madrid - Metro Francos Rodríguez L7, Estrecho L1

**Precio?** €20 cada taller, €70 si te apuntas a los cuatro. 

Para más info y apuntarse, pueden contactar conmigo (Valeria) por correo a **info@valeriatangomadrid.com** o tel al **+34** **655.225.930** (tb WhatsApp) **\-** **YA ESTÁ** **ABIERTA LA INSCRIPCIÓN - Plazas limitadas -**

- - -

**DINA MARTINEZ:**

A los 18 años me recibí de profesora Nacional de Danza Clásica. 
El clásico no era lo mío, pero sí enseñar. Cuando encontré el tango a mis 25 supe, sin dudas, a qué me iba a dedicar por el resto de mi vida.
En 1988 di mi primera clase de tango en un Centro Cultural del Gobierno de la Ciudad de Buenos Aires. 
Entre 1995 y 1997 integré el mítico grupo de investigación liderado por los maestros Gustavo Naveira y Fabián Salas: un momento de gran crecimiento en mi carrera.
En 1995 creé la Compañía de Tango Besos Tintos, que dirigí hasta el año 2022. Ya la extraño!!
En 2004 fundé junto a Luciana Valle y Valencia Batiuk, El Motivo Tango, un espacio de enseñanza y práctica que sigue siendo referencia obligada del tango actual en Buenos Aires.
Fui jurado de los Torneos Juveniles Bonaerenses durante 10 años, y del Campeonato Mundial en 2019.
Desde hace varios años viajo dando clases por diversas ciudades de Estados Unidos, Brasil y Europa.
En el mes de marzo tendré el placer de estar dando clases en las ciudades de Milán (Italia), Madrid, Barcelona y Gijón (España)- Querés ver más? [https://youtu.be/_751M4HbEeo](https://youtu.be/_751M4HbEeo)