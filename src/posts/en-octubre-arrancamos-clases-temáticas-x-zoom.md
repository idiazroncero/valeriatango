---
title: 'EN OCTUBRE, COREO-TANGO INDIVIDUAL!!!'
destacar: true
destacar_portada: false
featured_image: /public/images/coreo-tango-wa.jpeg
date: 2020-09-23T13:29:29.072Z
summary: CADA UNO EN SU CASA Y EL TANGO EN LA DE TODOS!!!
tags:
  - post
---
**COREO TANGO**: es un ciclo de 4 clases virtuales de 1 hora a la semana, en la que día a día iremos construyendo nuestra propia coreografía individual, pincelada con movimientos propios del tango y otros prestados de otras danzas. Para soltarnos y disfrutar de nuestro propio baile! 

En cada clase tomaremos una sección de un tema musical y trabajaremos sobre una serie de movimientos reiterados a los que iremos sumando variaciones. Sólo se requiere tener nociones básicas de baile, y pueden tomarlo chicos y chicas, ya que al ser coreo Individual, no hay diferenciación de roles. También se pueden tomar clases sueltas (no es necesario hacer el ciclo completo). **Primera clase 8 de Octubre!!! Anímate!!!**

- - -

**Cuándo?** Jueves de 20 a 21hs - Comenzamos el 8 de Octubre

**Dónde?** Online, vía Zoom

**Precio?** €10 la clase por persona.

Para participar es necesario apuntarse previamente, para que pueda facilitarles los datos de la reunión ( WhatsApp al **655.225.930** - Mail **info@valeriatangomadrid.com** )
