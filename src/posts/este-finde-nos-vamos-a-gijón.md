---
title: ESTE FINDE NOS VAMOS A GIJÓN!!!!
destacar: true
destacar_portada: false
featured_image: /public/images/publi-gijón-9.11.19.jpeg
date: 2019-11-07T12:01:12.411Z
tags:
  - post
---
**Hola amigos!!!** Este sábado, **9 de Noviembre**, tengo el placer de volver a Gijón, después de mucho tiempo, a dar dos talleres intensivos!!!



El primero será un **Taller de Técnica Femenina**, y la temática: **"LOS SECRETOS DEL PIVOT"**. Trabajamos en un principio con ejercicios de base de eje, equilibrio ,postura y traslado; y luego profundizaremos en las particularidades del pivot adelante y atrás, el sobrepivot, diferentes impulsos y variaciones de la técnica base.



El segundo Taller será **Para Ambos Roles de la Pareja**. La temática es **"EJE Y CIRCUNFERENCIA: QUIÉN RODEA A QUIÉN?"**. En este taller develaremos ese misterio!!! Y luego aplicaremos esos conceptos en diversos elementos del baile.



Para más info, o apuntarse, contactar con **Bego: 679.725.679**, o con **Marita: 653.556.992.**



**Muchísimas gracias a La Milonga que Faltaba, por organizarlo!! Nos vemos prontito!!!**
