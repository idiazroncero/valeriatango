---
title: ESTE SABADO, 22 DE ABRIL, TENEMOS YEITE!!!!
destacar: true
destacar_portada: false
featured_image: /public/images/el-yeite-logo.jpeg
date: 2023-01-25T11:39:15.089Z
summary: '¡¡¡SABADO 22 DE ABRIL: "EL YEITE, PRÁCTICA DE TANGO"!!!'
tags:
  - post
---
**¡¡¡HOLA A TODOS!!!** Cómo están? Queríamos contarles que este **SÁBADO, 22 de Abril, tenemos YEITE!!**

**Cuándo?** Sábado 4 de Marzo de 18 a 20hs (haremos una por mes) 

**Dónde?** Calle José Calvo 22. Metro Francos Rodríguez. 

**Precio:** 5 euros.

Los esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango ❤️ Proponemos un espacio de práctica, investigación y diversión, donde compartiremos algunos de "nuestros yeites"

**¡¡Importante!!** por cuestiones de aforo es necesario **reservar previamente**, escribiendo al **608239906/ 655225930**

Un abrazo fuerte y hasta el sábado!

**Caro y Vale**