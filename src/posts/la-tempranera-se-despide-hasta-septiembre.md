---
title: VOLVEMOS EN SEPTIEMBRE!!! QUE PASEN MUY BUEN VERANO!!!
destacar: true
destacar_portada: false
featured_image: /public/images/logo-la-tempranera.jpg
date: 2019-06-18T08:13:02.719Z
summary: ''
tags:
  - post
---
En los meses de Julio y Agosto de 2019 la sala estará cerrada por obras, y vacaciones. **EN SEPTIEMBRE** **RETOMAREMOS TODAS NUESTRAS ACTIVIDADES: clases regulares, privadas, talleres intensivos y Práctica "La Tempranera".** Pero seguimos disponibles por mail o WhatsApp, por cualquier consulta.

**¡¡¡QUE PASEN UN MUY HERMOSO VERANO!!!** 

- - -

**¿QUÉ ES "LA TEMPRANERA"?**

Es una **PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

**La Tempranera** está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una **Práctica Guiada** porque yo, Valeria, estoy presente y disponible para responder a las dudas que surjan en el baile, y corregir individualmente a quien lo necesite.

**Cuándo?** El último **viernes** de cada mes, de **11 a 13:30hs.** 

**Dónde?** En El Portón Rojo, Calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

**Precio?** "A la Gorra", ponemos un sombrero donde cada uno deja lo que puede, si puede, y si no, nada!
