---
title: '31/5: ORÍGENES Y EVOLUCIÓN DEL TANGO CON RAFA FLORES EN "LA TEMPRANERA"!! '
destacar: true
destacar_portada: false
featured_image: /public/images/tempranera-31.5.19.jpeg
date: 2019-02-28T10:59:58.385Z
summary: ''
tags:
  - post
---
**LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

**La Tempranera** está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una **Práctica Guiada** porque yo, Vale, estoy presente y disponible para responder a las dudas que surjan, y corregir individualmente si alguien lo necesita.

En la próxima **Tempranera, el** **31 de Mayo**, tendremos un invitado de honor, el señor **Rafael Flores Montenegro**, escritor e investigador de tango, quien nos regalará una de sus "necesarias" charla sobre Orígenes y Evolución del Tango. Un lujazo!!! 

**Cuándo?** El último **viernes** de cada mes, de **11 a 13:30hs. Próximas Tempraneras:** 31 de Mayo y 28 de Junio.

**Dónde?** En El Portón Rojo, Calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

**Precio?** "A la Gorra", un sombrero donde cada uno pone lo que puede, si puede, y si no, nada!
