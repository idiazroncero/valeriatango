---
title: 'OCTUBRE Y NOVIEMBRE, CICLO DE TÉCNICA FEMENINA!!!'
destacar: true
featured_image: /public/images/técnica-fem-wa.jpeg
date: 2020-09-24T10:38:32.119Z
summary: 'CLASES SEMANALES Y DESDE CASA!!! '
tags:
  - post
---
Es un ciclo de 8 clases virtuales de 1 hora semanal en la que trabajaremos en profundidad los elementos de tango propios del rol seguidor.

En cada clase haremos un primer trabajo de base (postural, eje, tonicidad) para luego desarrollar el tema específico correspondiente a esa clase (caminata, pivot, boleos, adornos…). La periodicidad de este Ciclo de Técnica favorece la incorporación de nuevos gestos y posturas, gracias a su repetición semana a semana. Se pueden tomar clases sueltas, no es necesario hacer el ciclo completo. **Primera clase 6 de Octubre!!! Anímate!!!**

- - -

**Cuándo?** Hay dos horarios a elegir

Martes de 20 a 21hs - Primera clase el 6 de Octubre

Jueves de 11 a 12hs - Comenzamos el 8 de Octubre

**Dónde?** Online, vía Zoom

**Precio?** €10 la clase por persona 

Para participar es necesario apuntarse previamente, para que pueda facilitarles los datos de la reunión ( WhatsApp al **655.225.930** - Mail **info@valeriatangomadrid.com** )
