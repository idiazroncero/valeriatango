---
title: SE VIENE "EL YEITE", CON CLASE PREVIA DE MILONGA
destacar: false
destacar_portada: false
featured_image: /public/images/yeite-4.24.jpeg
date: 2024-04-18T14:38:06.429Z
summary: ¡¡¡HORARIO DE VERANO!!!
tags:
  - post
---
**Nuevo "YEITE"** con **HORARIO DE VERANO** y **CLASE PREVIA!!!!** 

**¡¡¡SÁBADO 27 de Abril!!!❤️**

**17 a 19hs**. Clase con Caro y Vale: **"MILONGA"**\
**19 a 21hs.** **"EL YEITE"**, Práctica de Tango

**Dónde?** Calle José Calvo 22, Madrid. Metro Francos Rodríguez.
**Precio:** Clase + Práctica €30/Práctica sola €5.
Te esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!!
Importante!! por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como  a la práctica): **608239906/ 655225930**

Abrazo❤️

**Caro y Vale**