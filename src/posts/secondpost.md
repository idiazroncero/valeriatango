---
title: '19/01: TALLER DE CONEXIÓN!!!'
destacar: true
destacar_portada: false
featured_image: /public/images/conexión-19.1.2019.jpeg
date: 2019-01-12T00:00:00.000Z
summary: >-
  Sábado 19 de Enero, Taller de Técnica: CONEXIÓN, DESDE EL ABRAZO A LA PIERNA
  LIBRE (un camino de ida y vuelta). 
tags:
  - post
  - sport
---
Sábado 19 de Enero, Taller de Técnica: CONEXIÓN, DESDE EL ABRAZO A LA PIERNA LIBRE (un camino de ida y vuelta). Trabajaremos cómo conectar el propio cuerpo (brazos/torso, cadera/pierna base/pierna libre, dibujos posibles con la pierna libre) y la conexión con el compañero (puntos de empuje y resistencia en traslado, pívot y giro, y variaciones según el tipo de abrazo).
