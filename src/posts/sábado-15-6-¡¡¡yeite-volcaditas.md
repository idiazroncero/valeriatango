---
title: "Sábado 15/6: ¡¡¡YEITE + VOLCADITAS!!!"
destacar: false
destacar_portada: false
featured_image: /public/images/yeite-15.6.24.jpeg
date: 2024-06-06T14:04:53.103Z
summary: -ULTIMO YEITE DE ESTE CURSO-
tags:
  - post
---
**¡Hola amig@s!** ¿Cómo están? Con mucha alegría quiero contarles que el próximo sábado **15 de Junio**, ¡¡tenemos **YEITE!!!** Será el último de esta temporada, pero a no desesperar, que retomaremos después de verano!! Y en la clase previa vamos a trabajar **APILES Y VOLCADITAS MILONGUERAS** (es decir, que se puedan hacer en la pista de baile). Esta temática está pensada para ambos roles de la pareja, por lo que reacomodamos apuntarse en parejas. Si no tienen compi, avísennos y trataremos de coordinar leaders y followers. Aquí abajo va toda la info. **¡¡¡Los esperamos!!!**



- - -



**¡¡¡SÁBADO 15 de JUNIO: "YEITE" + CLASE PREVIA!!!❤️**

**17 a 19hs**. Clase con Caro y Vale: **"APILES Y VOLCADITAS MILONGUERAS"**\
**19 a 21hs.** **"EL YEITE"**, Práctica de Tango.

**Dónde?** Calle José Calvo 22, Madrid. Metro Francos Rodríguez.
**Precio:** Clase + Práctica €30/Práctica sola €5.
Los esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!!
Importante!! por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como  a la práctica): **608239906/ 655225930**

Un abrazo❤️

**Caro y Vale**