---
title: '16/02: EL BOLEO Y SUS VERICUETOS!'
destacar: true
destacar_portada: false
featured_image: /public/images/boleos.jpeg
date: 2019-02-08T00:00:00.000Z
summary: '    5º Taller de Técnica Femenina del Ciclo TANGO APERITIVO!'
tags:
  - Clases de tango
  - tango en Madrid
  - Intensivos
  - Técnica Femenina
  - Talleres de Técnica Femenina
  - Cursos de tango
  - ''
---
Sábado 16 de Febrero, 5° TALLER DE TÉCNICA FEMENINA! La temática? "EL BOLEO Y SUS VERICUETOS". Trabajaremos el principio de contradirección, los distintos tipos de boleos: circulares atrás, adelante, lineales; y las diferentes calidades de boleos según el impulso y la rítmica propuesta. Para más info y apuntarse contáctame a info@valeriatangomadrid.com o al 655.225.930 (también WhatsApp) - Plazas Limitadas -
