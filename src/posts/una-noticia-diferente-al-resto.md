---
title: POR AFORO COMPLETO REPETIMOS TALLER DE BOLEOS!!
destacar: true
destacar_portada: false
featured_image: /public/images/boleos_2.jpg
date: 2019-02-11T12:17:40.045Z
summary: ''
tags:
  - ''
---
Sábado 16 de Febrero, 5° TALLER DE TÉCNICA FEMENINA! La temática? "EL BOLEO Y SUS VERICUETOS". Trabajaremos el principio de contradirección, los distintos tipos de boleos: circulares atrás, adelante, lineales; y las diferentes calidades de boleos según el impulso y la rítmica propuesta. Para más info y apuntarse contáctame a info@valeriatangomadrid.com o al 655.225.930 (también WhatsApp) - Plazas Limitadas -
