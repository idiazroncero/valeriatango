---
title: 'VIERNES 28/2: "LA TEMPRANERA"'
destacar: true
destacar_portada: false
featured_image: /public/images/logo-la-tempranera.jpg
date: 2020-02-26T11:33:13.434Z
summary: '- PRÁCTICA GUIADA DE TANGO POR LAS MAÑANAS -'
tags:
  - post
---
Este viernes, **28 de Febrero**, tendremos una nueva edición de **"LA TEMPRANERA"**. Los esperamos de **11 a 14**hs, para seguir "sacando viruta al piso"!!!

- - -

**LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**.  Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

La Tempranera está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una Práctica Guiada porque yo, Vale, estoy presente y disponible para responder a las dudas que surjan, y corregir individualmente si alguien lo necesita.

Cuándo? El último viernes de cada mes, de 11 a 14hs.

Dónde? En El Portón, Calle José Calvo 22, Madrid - Metro Francos Rodriguez L7, Estrecho L1

Precio? "A la Gorra", un sombrero donde cada uno pone lo que puede, si puede!
