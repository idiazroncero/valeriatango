---
title: ¡EL 9 DE SEPTIEMBRE RETOMAMOS!!!
destacar: false
destacar_portada: false
featured_image: /public/images/horario-curso-24-25.jpeg
date: 2024-09-04T12:12:33.431Z
summary: ¡VUELTA AL COLE!
tags:
  - post
---
**¡Hola, amig@s!** ¿Cómo están? ¿Cómo han pasado el verano? ¡Espero que muy bien, disfrutando y cargando pilas!  Les cuento las actividades tangueras que tengo para ofrecerles este nuevo curso. 

¡El **9 de septiembre** retomo todas las actividades! Esa primera semana haré dos **PRÁCTICAS DE REENCUENTRO**: el **lunes 9/9 de 20 a 22hs,** y el **jueves 12/9 de 11 a 12:30hs**  Estas prácticas están pensadas principalmente para los alumnos del curso pasado, pero estarán abiertas a todo aquel que quiera venir a conocer el espacio, y bailar unos tanguitos. Serán gratuitas, sólo tienen que avisarme por WhatsApp a **655225930**, para que los tenga en cuenta. 

El **miércoles 11 de septiembre de 19:30 a 21hs**, daré una **CLASE ABIERTA** de Iniciación al tango, para todos los que quieran acercarse a ver de qué se trata, o que ya bailen y quieran refrescar las bases. (también es gratuita, sólo tienen que avisarme)  

A partir de la semana del **16 de septiembre**, arrancamos con las **clases regulares para Principiantes, Intermedios I, e Intermedios II**, tanto por las tardes como por las mañanas. Aquí abajo va la Info:

**LUNES**

19:30 a 20:30Hs.........**Intermedios I**

20:30 a 21hs...............**Práctica**

21 a 22hs.....................**Intermedios II**

**MIÉRCOLES** 

19:30 a 20:30hs.........**Iniciación al Tango**

20:30 a 21hs...............**Práctica**

**JUEVES**

11 a 12hs....................**Intermedios**

12 a 12:30hs.............**Práctica**

12:30 a 13:30hs.......**Iniciación al Tango** (a partir de Octubre)



**Dónde?** Calle José Calvo 22, Madrid (metro Francos Rodriguez L7 o Estrecho L1, un poquito más lejos)

**Precio**: 

Iniciación €45 la mensualidad por persona (clase + práctica)

Intermedios €50 la mensualidad por persona (clase + práctica)

**Para más info** o apuntarte puedes contactarme al+**34 655.225.930**