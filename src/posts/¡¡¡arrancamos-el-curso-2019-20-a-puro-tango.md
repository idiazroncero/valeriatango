---
title: ARRANCAMOS EL CURSO 2019/20 A TODO TANGO!!!
destacar: true
destacar_portada: false
featured_image: /public/images/publi-clase-abierta-30.9.19.jpeg
date: 2019-09-12T09:53:29.085Z
tags:
  - post
---
En Septiembre retomamos las Clases Regulares y Privadas, y en Octubre los Talleres Intensivos y la Práctica.

**CLASE ABIERTA Y GRATUITA DE INICIACIÓN AL TANGO**: lunes 30 de SEPTIEMBRE de 11 a 12:30hs. Puedes reservar tu plaza desde ahora! info@valeriatangomadrid.com - 655.225.930 (tb WhatsApp)

¡Para que nadie se quede con las ganas de bailar!!!

**FECHAS DE COMIENZO DE TODAS LAS ACTIVIDADES**:

**Grupales por la noche:** miércoles 4 de Septiembre - [Yolobailo](https://valeriatangomadrid.com/clases/iniciacion-al-tango-ii/) -

**Grupales por las mañanas:** lunes 23 de Septiembre - El Portón - C/José Calvo, 22 - Tetuán .-

**Privadas:** a partir del lunes 16 de Septiembre - El Portón - 

**Talleres Intensivos:** Primer taller del Ciclo, sábado 19 de Octubre - El Portón -

**Práctica Guiada La Tempranera:**  viernes 25 de Octubre - El Portón - 

Para más info, contáctame por WhatsApp o tel al **655.225.930**, o por correo a info@valeriatangomadrid.com!
