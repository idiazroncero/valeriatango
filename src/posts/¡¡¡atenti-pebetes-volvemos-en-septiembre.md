---
title: "¡¡¡ATENTI PEBETES: VOLVEMOS EN SEPTIEMBRE!!!"
destacar: false
destacar_portada: false
featured_image: /public/images/valeria_backstage-50.jpg
date: 2024-08-21T07:14:02.267Z
summary: PREPARANDO LA TEMPORADA 24-25
tags:
  - post
---
**¡¡¡Hola amigos!!!** ¿Cómo están? ¿Cómo han pasado el veranito? Esperamos que muy bien, disfrutando y cargando pilas. Por aquí, ya estamos de vuelta, en plena organización. Para este curso 24-25, tenemos nuevas propuestas de clases, talleres y actividades tangueras para ofrecerles. En breve pondremos por aquí la info completa, pero para que vayan agendando, retomaremos actividades la semana del **9 de Septiembre**. 

Y si tienen alguna pregunta o comentario, no duden en llamarme al **655.225.930** (también WhatsApp) o escribirme a **info@valeriatangomadrid.com**

¡¡¡Nos vemos muy prontito!!!

Un abrazo bien tanguero.

Vale