---
title: ¡¡¡ESTE MES EL YEITE CAMBIA DE DÍA!!!
destacar: false
destacar_portada: false
featured_image: /public/images/el-yeite-logo.jpeg
date: 2023-06-12T12:14:52.002Z
summary: DOMINGO 18 DE JUNIO DE 18 A 22HS
tags:
  - post
---
**¡¡¡HOLA AMIGOS!!!** Cómo están? Este mes de Junio, tenemos que mover de día nuestra querida práctica de "**EL YEITE". La haremos este próximo domingo, 18 de junio en el horario habitual de 18 a 20hs!!!** Esperamos que no les traiga mayores inconvenientes, y que puedan venir todos! **¡¡¡Éste será el último Yeite de este curso!!!** Después de verano volveremos como siempre, un Sábado al mes de 18 a 20hs.

**Cuándo?** Domingo 18 de Junio ´23 de 18 a 20hs 

**Dónde?** Calle José Calvo 22. Metro Francos Rodríguez.

**Precio:** 5 euros.

Los esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango ❤️ Proponemos un espacio de práctica, investigación y diversión, donde bailar, disfrutar y compartir "nuestros yeites"

**¡¡Importante!!** por cuestiones de aforo es necesario **reservar previamente**, escribiendo al **608239906/ 655225930**

Un abrazo fuerte y hasta el domingo!

**Caro y Vale**