---
title: ¡¡¡ESTE SÁBADO ARRANCAMOS CON "EL YEITE"!!!
destacar: false
destacar_portada: false
featured_image: /public/images/publi-el-yeite-19.11.22.jpeg
date: 2022-11-16T11:48:31.667Z
summary: ¡¡¡NUEVA PRÁCTICA GUIADA DE TANGO EN MADRID!!!
tags:
  - post
---


**Nueva Práctica de Tango en Madrid**: **"EL YEITE"**❤️

Estamos muy felices de poder anunciarles el inicio de nuestra nueva práctica, EL YEITE. La organizamos con mucho cariño Carolina Gonzales y Valeria Cuenca, aquí compartimos toda la data:


**Cuándo?** Sábado 19 de noviembre de 18 a 20 hs (haremos una por mes)
**Dónde?** Calle José Calvo 22. Metro Francos Rodríguez.
**Precio:** 5 euros.


Los esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango ❤️
Proponemos un espacio de práctica, investigación y diversión, donde compartiremos algunos de "nuestros yeites" 


**¡¡Importante!!** por cuestiones de aforo es necesario **reservar previamente**, escribiendo al **608239906/ 655225930**

Un abrazo fuerte y hsta el sábado!



Caro y Vale