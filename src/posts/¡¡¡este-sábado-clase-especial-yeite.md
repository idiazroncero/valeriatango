---
title: ¡¡¡ESTE SÁBADO, CLASE ESPECIAL + YEITE!!!
destacar: false
destacar_portada: false
featured_image: /public/images/whatsapp-image-2024-01-19-at-19.11.33.jpeg
date: 2024-01-24T15:05:55.681Z
summary: SABADO 27 DE ENERO
tags:
  - post
---
**¡¡¡NOTICIA, NOTICIÓN!!!** 📣📣📣 Con el 2024 vuelve **"EL YEITE"** y esta vez viene con **CLASE PREVIA!!!!** 🥳🥳🥳 

**¡¡¡SÁBADO 27 de ENERO!!!❤️**

**16 a 18hs**.Clase Especial con Caro y Vale: **"TRUCOS PARA LA PISTA"**-Jugando con los cambios de peso.\
**18 a 20hs.** **"EL YEITE"**, Práctica de Tango

**Dónde?** Calle José Calvo 22, Madrid. Metro Francos Rodríguez.
**Precio:** Clase + Práctica €30/Práctica sola €5.
Te esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!!
Importante!! por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como  a la práctica): **608239906/ 655225930**


Abrazo❤️


**Caro y Vale**