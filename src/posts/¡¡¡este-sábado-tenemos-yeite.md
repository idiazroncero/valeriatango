---
title: ¡¡¡ESTE SÁBADO TENEMOS YEITE!!!
destacar: true
destacar_portada: false
featured_image: /public/images/yeite-30.11.24.jpeg
date: 2024-11-26T12:47:45.218Z
summary: "- ULTIMO YEITE DEL 2024 -"
tags:
  - post
---


**Sábado 30/11 "EL YEITE"** con **CLASE PREVIA!!!**❤️

 

**17 a 19hs** Clase con Caro y Vale: **"La fuerza del tiempo débil"**

 

**19 a 21hs. "EL YEITE"** Práctica de Tango ¡¡¡Último Yeite del año!!!(retomamos en enero)

 

**Dónde?** Calle José Calvo 22, Madrid. Metro Francos Rodríguez. 

**Precio:** Clase + Práctica €30/Práctica sola €5. 

Te esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!! **¡¡Importante!!** por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como a la práctica): **608239906/ 655225930**

 

Un abrazo❤️

 

**Caro y Vale**