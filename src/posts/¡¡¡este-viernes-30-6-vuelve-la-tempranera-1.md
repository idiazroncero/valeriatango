---
title: ¡¡¡ESTE VIERNES 9/2, VUELVE LA TEMPRANERA!!!
destacar: false
destacar_portada: false
featured_image: /public/images/tempranera-24.2.9.jpeg
date: 2024-02-05T13:18:44.343Z
summary: --¡Y LA CHARLA DE RAFA FLORES!--
tags:
  - post
---
**Hola amigos!** Cómo están? Tengo la enorme alegría de contarles que este **viernes 9 de Febrero**, haremos una nueva edición de nuestra Práctica de Tango Matutina **"La Tempranera"**. Y en esta oportunidad, contamos con un evento muy especial...nuestro querido **Rafael Flores** nos dará una de sus maravillosas e instructivas charlas. Esta vez nos hablará sobre las **Orquestas de Miguel Caló, Osvaldo Pugliese y Juan D'Arienzo** **¡¡¡Imperdible!!!** Aquí abajo va toda la data. Los esperamos este viernes!!!! Abrazos!

- - -

**LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

**De 11 a 12hs, será la Charla de Rafa Flores, y luego, de 12 a 14hs, haremos la Práctica, en la que yo estaré atenta a resolver cualquier duda que surja en el baile.**

**Cuándo?** Viernes 9 de Febrero, de 11 a 12hs, charla con Rafa Flores, y de 12 a 14hs, Práctica

**Dónde?** Calle José Calvo 22, Madrid (Metro Francos Rodríguez L7, Estrecho L1)

**Precio?** €5 por persona.

Saludos. 

Vale