---
title: ¡¡¡ESTE VIERNES, 30/6, VUELVE LA TEMPRANERA!!!
destacar: true
destacar_portada: false
featured_image: /public/images/la-tempranera-30.6.23.jpeg
date: 2023-06-28T06:44:06.284Z
summary: LA PRÁCTICA DE TANGO MATUTINA
tags:
  - post
---
**Hola amigos!** Cómo están? Tenemos la enorme alegría de contarles que este **viernes 30 de Junio**, después de 3 años, volvemos a hacer nuestra práctica de tango matutina **"La Tempranera"**. Y en esta oportunidad, contamos con un evento muy especial...nuestro querido **Rafael Flores** nos dará una de sus maravillosas e instructivas charlas sobre el tango y sus vericuetos. **¡¡¡Imperdible!!!** De esta manera despediremos el curso 22/23. Pero...a partir de Septiembre, retomaremos con ´más clases, más Yeites y, por supuesto, más Tempraneras para seguir disfrutando y compartiendo tango!!! Aquí abajo va toda la data. Los esperamos este viernes!!!! Abrazos!

- - -

**LA TEMPRANERA, PRÁCTICA GUIADA DE TANGO POR LA MAÑANA**. Un espacio familiar, donde practicar lo aprendido, probar cosas nuevas, intercambiar ideas con nuestros compañeros, compartir y divertirnos; acompañado de un cafecito y algo rico para picar!!

La Tempranera está abierta a todo aquel que quiera venir a bailar y disfrutar (no está limitada sólo a alumnos). Es una Práctica Guiada porque Caro y yo, Vale, estaremos presente y disponible para responder a las dudas que surjan, y corregir individualmente si alguien lo necesita.

**Cuándo? Viernes 30 de Junio, de 11 a 12hs, charla con Rafa Flores, y de 12 a 14hs, Práctica**

**Dónde?** Calle José Calvo 22, Madrid (Metro Francos Rodriguez L7, Estrecho L1)

**Precio?** €5 por persona.