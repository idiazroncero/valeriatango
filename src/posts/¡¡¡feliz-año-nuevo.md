---
title: ¡¡¡FELIZ AÑO NUEVO!!!
destacar: false
destacar_portada: false
featured_image: /public/images/findeaño24.jpg
date: 2024-01-01T08:13:27.600Z
tags:
  - post
---
Esperamos que este año que recién comienza nos traiga paz, abrazos y muchos más tangos compartidos.

¡¡MUY FELIZ 2024!!



Nos hemos tomado un pequeño receso por las Navidades. Retomaremos todas las clases el LUNES 8 de enero.



Un abrazo.