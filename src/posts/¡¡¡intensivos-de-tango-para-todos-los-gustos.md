---
title: ¡¡¡INTENSIVOS DE TANGO PARA TODOS LOS GUSTOS!!!
destacar: false
destacar_portada: false
featured_image: /public/images/intensivos-julio-publi.jpeg
date: 2024-06-26T21:48:35.018Z
summary: "- DEL 1 AL 4 DE JULIO -"
tags:
  - post
---
**¡Hola amigos!** ¿Cómo están? Ya se viene el veranito, y con él, el tiempo las vacaciones! Terminamos las clases regulares, y con mucha alegría y agradecimiento porque fue un curso hermoso! Peeroo, todavía nos queda una semanita especial, de **intensivos**, y con propuestas diferentes para todos los gustos! Aquí abajo va toda la info. Espero que les gusten!

Ya a partir de mediados de septiembre, retomamos las clases regulares, con nuevos cursos, y por supuesto, nuestras queridas prácticas del Yeite con Caro (y con clase previa), y la Tempranera. Nos vemos prontito!!! 

Un abrazo fuerte!

Vale

- - -

**¡¡¡INTENSIVOS DE VERANO CON VALE!!!**
       -Del 1 al 4 de Julio de 20 a 22hs-

**\-Lunes 1/7: ANÁLISIS DE MOVIMIENTOS** (cómo entender el baile) - Para ambos roles - Nivel Medio

**\-Martes 2/7: TECNICA DE BASE-**Técnica individual para ambos roles - Todos los niveles

**\-Miércoles 3/7: ADORNOS-** parafollowers - Nivel medio

**\-Jueves 4/7: INICIACIÓN AL TANGO** - Para ambos roles - Nivel Inicial

**Dónde?** en Calle José Calvo, 22-Madrid
**Precio:** €25 por persona cada intensivo Promo Iniciación al tango: si se apuntan en pareja, €45 entre los dos.

Para que salga adelante cada taller, necesito que haya un mínimo de personas apuntadas, por lo que pido una reserva de plaza de €10 por Bizum, y que se apunten hasta el viernes 28/6 como fecha limite. Gracias!!! 

Para apuntarse, y por cualquier otra consulta, me pueden contactar por WhatsApp al **655.225.930**

¡Nos vemos prontito!