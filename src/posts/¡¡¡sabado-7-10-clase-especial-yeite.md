---
title: "7/10: CLASE ESPECIAL + YEITE CANCELADAS"
destacar: false
destacar_portada: false
featured_image: /public/images/yeite-cancelado.png
date: 2023-10-07T10:21:01.080Z
tags:
  - post
---
**AMIGOS, POR RAZONES DE FUERZA MAYOR, HEMOS TENIDO QUE CANCELAR LA CLASE Y LA PRÁCTICA PREVISTAS PARA HOY, 7 DE OCTUBRE. LES AVISAREMOS CUANDO PODAMOS RETOMARLO. UN ABRAZO Y DISCULPEN LAS MOLESTIAS.** 



¡¡¡NOTICIA, NOTICIÓN!!! 📣📣📣Vuelve "EL YEITE" y esta vez viene con CLASE PREVIA!!!! 🥳🥳🥳 

¡¡¡SÁBADO 7 de OCTUBRE!!!❤️

16:30 a 18:30hs.Clase Especial con Caro y Vale: "TRUCOS PARA LA PISTA"\
18:30 a 20:30hs "EL YEITE", Práctica de Tango

Dónde? Calle José Calvo 22, Madrid. Metro Francos Rodríguez.
Precio: Clase + Práctica €30/Práctica sola €5.
Te esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!!
Importante!! por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como  a la práctica): 608239906/ 655225930
Abrazo❤️
Caro y Vale