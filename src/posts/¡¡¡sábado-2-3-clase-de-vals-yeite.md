---
title: "¡¡¡SÁBADO 2/3: CLASE DE VALS + YEITE!!!"
destacar: false
destacar_portada: false
featured_image: /public/images/yeites-para-la-pista_vals.jpg
date: 2024-02-19T12:37:47.452Z
summary: 16 a 18hs. Clase/18 a 20hs. Práctica
tags:
  - post
---
**Nuevo "EL YEITE"** con **CLASE PREVIA!!!!** 

**¡¡¡SÁBADO 2 de MARZO!!!❤️**

**16 a 18hs**. Clase Especial con Caro y Vale: **"TRUCOS PARA LA PISTA: Yeites para el Vals Criollo"**\
**18 a 20hs.** **"EL YEITE"**, Práctica de Tango

**Dónde?** Calle José Calvo 22, Madrid. Metro Francos Rodríguez.
**Precio:** Clase + Práctica €30/Práctica sola €5.
Te esperamos para compartir una tardecita de mates, cafecito, abrazos y mucho Tango!!
Importante!! por cuestiones de aforo es necesario reservar previamente (tanto para apuntarse a la clase como  a la práctica): **608239906/ 655225930**

Abrazo❤️

**Caro y Vale**