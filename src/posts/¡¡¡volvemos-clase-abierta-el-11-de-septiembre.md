---
title: ¡¡¡VOLVEMOS!!! CLASE ABIERTA EL 11 DE SEPTIEMBRE
destacar: false
destacar_portada: false
featured_image: /public/images/clase-abierta-11.9.24.jpeg
date: 2024-09-04T12:49:01.009Z
summary: INICIACIÓN AL TANGO
tags:
  - post
---
**Hola amig@s!** Cómo están? Les quería contar que el próximo **miércoles 11 de Septiembre de 19:30 a 21hs**, daré una **Clase Abierta de Iniciación al Tango**, para aquellos que quieran saber de qué se trata. La idea, a partir de ahí es armar un nuevo grupo de Nivel Inicial en el mismo día y horario (si se forma grupo). Pero a la clase abierta, están todos invitados a participar, sin ningún compromiso. Sólo necesito que me avisen por WhatsApp al **+34 655.225.930**, para tenerlos en cuenta.

Y el **16 de Septiembre** arrancamos con todas las **Clases Grupales.** Aquí les dejo la info completa:

**LUNES**

19:30 a 20:30Hs.........**Intermedios I**

20:30 a 21hs...............**Práctica**

21 a 22hs.....................**Intermedios II**

**MIÉRCOLES**

19:30 a 20:30hs.........**Iniciación al Tango**

20:30 a 21hs...............**Práctica**

**JUEVES**

11 a 12hs....................**Intermedios**

12 a 12:30hs.............**Práctica**

12:30 a 13:30hs.......**Iniciación al Tango** (a partir de Octubre)

**Dónde?** Calle José Calvo 22, Madrid (metro Francos Rodriguez L7 o Estrecho L1, un poquito más lejos)

**Precio**:

Iniciación: €45 la mensualidad por persona (clase + práctica)

Intermedios: €50 la mensualidad por persona (clase + práctica)

**Para más info** o apuntarse pueden contactarme al+**34 655.225.930**