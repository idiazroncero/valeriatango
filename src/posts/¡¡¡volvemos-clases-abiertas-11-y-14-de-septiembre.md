---
title: ¡¡¡VOLVEMOS!!!     CLASES ABIERTAS 11 Y 14 DE SEPTIEMBRE
destacar: false
destacar_portada: false
featured_image: /public/images/publi-tango-9.23.jpeg
date: 2023-09-04T07:10:53.140Z
summary: Lunes 11/9 19:30hs, y Jueves 14/9 10:30hs clases abiertas y gratuitas
tags:
  - post
---
**¡Hola amigos!** Espero que hayan pasado un muy buen verano. Por aquí ya estoy de vuelta preparando las nuevas propuestas tangueras para este curso! Lo primero, comentarles que por el momento **Caro González**, no va a poder continuar dando las clases regulares conmigo (espero que pronto se pueda reincorporar!). Por otro lado, contarles que tenemos una nueva integrante en el equipo, **Gala** se suma como ayudante en las clases. ¡Bienvenida!

Y ahora sí, contarles que la semana que viene inauguramos la temporada tanguera con dos **clases abiertas y gratuitas**, a las que están todos invitados: el **lunes 11/9 de 19:30 a 21hs, y el jueves 14/9 de 10:30 a 12hs.** Y a partir de ahí retomamos las clases regulares con mucha alegría e ilusión! Aquí abajo va la info detallada. ¡Los esperamos muy prontito!!

* **CLASES ABIERTAS: Lunes 11/9 de 19:30hs y Jueves 14/9 de 10:30 a 12hs.** Por favor apúntate previamente para reservar tu lugar!


* **CLASES GRUPALES:** \
  **LUNES** 19:30 a 20:30hs......**INICIACIÓN al TANGO**\
               20:30 a 21hs...........**PRÁCTICA CONJUNTA**

\    21 a 22hs................**NIVEL INTERMEDIO**

 **\    JUEVES** 10:30 a 11:30hs....**INICIACIÓ​N AL TANGO**\
                      11:30 a 12hs..........**PRÁCTICA CONJUNTA**

12 a 13hs...............**NIVEL INTERMEDIO (comienza el 5 de Octubre)**

**Todas las clases grupales comienzan la semana del 18 de Septiembre, excepto la de nivel intermedio de los jueves que empieza en Octubre**

**\
Dónde?** Calle José Calvo 22, Madrid (metro Francos Rodriguez L7 o Estrecho L1, un poquito más lejos)

**Precio**: Clase abierta, gratuita

Iniciación €45 la mensualidad por persona (clase + práctica conjunta)

Intermedios €50 la mensualidad por persona (clase + práctica)

**Para más info** o apuntarte puedes contactarme al+**34 655.225.930**